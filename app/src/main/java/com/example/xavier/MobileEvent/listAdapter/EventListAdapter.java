package com.example.xavier.MobileEvent.listAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.xavier.MobileEvent.model.Event;
import com.example.xavier.MobileEvent.R;

import java.util.List;

/**
 * Created by x
 */

public class EventListAdapter extends BaseAdapter {
    private List<Event> eventList;
    private LayoutInflater inflater;

    public EventListAdapter(Context context, List<Event> listE){
        this.inflater=LayoutInflater.from(context);
        this.eventList =listE;
    }
    @Override
    public int getCount() {
        return this.eventList.size();
    }

    @Override
    public Object getItem(int i) {
        return this.eventList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return this.eventList.get(i).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView txtEvent;
        if(convertView==null){
            convertView = this.inflater.inflate(R.layout.item_event,parent,false);
            txtEvent = convertView.findViewById(R.id.txt_evt);
            convertView.setTag(R.id.txt_evt,txtEvent);
        }
        else{
            txtEvent = (TextView) convertView.getTag(R.id.txt_evt);
        }
        Event evt = (Event) this.getItem(position);
        txtEvent.setText(evt.getTitre());
        return convertView;
    }

}
