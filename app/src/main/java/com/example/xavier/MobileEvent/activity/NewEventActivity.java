package com.example.xavier.MobileEvent.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.xavier.MobileEvent.R;

/**
 * Created by x
 */
public class NewEventActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_event);
        Button btnSave = findViewById((R.id.btnSave));
        btnSave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent();
                switch (v.getId()) {
                    case R.id.btnSave:
                        EditText txt_newEvt = findViewById(R.id.txt_newEvent);
                        i.putExtra("titreEvt", txt_newEvt.getText().toString());
                        setResult(Activity.RESULT_OK,i);
                        finish();
                        break;
                }
                setResult(Activity.RESULT_CANCELED,i);
                finish();
            }
        });
    }
}
